const web = require("express").Router()
const HomeController = require("../controllers/web/HomeController");
const AuthController = require("../controllers/web/AuthController");
const homeController = new HomeController();
const authController = new AuthController();
const bodyParser = require("body-parser");
const passport = require("../lib/passport");
const Restrict = require("../middlewares/restrict");

web.use(bodyParser.json());

web.get("/login", authController.login);
web.post('/login', passport.authenticate('local',{
    successRedirect: '/',
    failureRedirect: '/login',
    failureFlash: true
}))

web.use(Restrict)

web.get("/", homeController.dashboard)
web.get("/admin", homeController.listAdmin)
web.get("/editAdmin/:id", homeController.editAdmin)
web.post("/editAdmin/:id", homeController.saveAdmin)
web.get("/delete/:id", homeController.deleteAdmin); 

web.get("/history", homeController.history); 

web.get("/register", homeController.addAdmin); 
web.post("/register", authController.register);
web.get("/logout", authController.logout);

web.get("/users", homeController.getUser);

module.exports = web;
