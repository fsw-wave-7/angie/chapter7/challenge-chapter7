const { Admin } = require("../../models");
const passport = require("../../lib/passport");
const bcrypt = require('bcrypt')
class AuthController {
  login = (req, res) => {
    res.render("login", {
      judul: "login",
      content: "./content/login",
    });
  };

  whoami = (req, res) => {
    res.render("index", req.user.dataValues);
  };

  logout = (req, res) => {
    req.logout();
    res.redirect("/login");
  };

  register = (req, res) => {
    Admin.register(req.body).then(() => {
      res.redirect("/admin");
    });
  };
}

module.exports = AuthController;
