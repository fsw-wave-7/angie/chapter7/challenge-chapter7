const { Admin, User, History, Biodata } = require("../../models");
const bcrypt = require("bcrypt");

class HomeController {
  dashboard = (req, res) => {
    User.findAll({
      include: [
        {
          model: History,
          as: "history",
        },
      ],
      order: [["createdAt", "DESC"]],
    }).then((user) => {
      res.render("index", {
        judul: "Dashboard",
        content: "./content/dashboard",
        pengguna: user,
        admin: req.user.dataValues,
      });
    });
  };

  addAdmin = (req, res) => {
    res.render("index", {
      judul: "Add Admin",
      content: "./content/addAdmin",
      admin: req.user.dataValues,
    });
  };

  listAdmin = (req, res) => {
    Admin.findAll().then((admin) => {
      res.render("index", {
        judul: "List Admin",
        content: "./content/listAdmin",
        users: admin,
        admin: req.user.dataValues,
      });
    });
  };

  editAdmin = (req, res) => {
    const id = req.params.id;
    Admin.findOne({
      where: {
        id: id,
      },
    }).then((admin) => {
      res.render("index", {
        judul: "Edit Admin",
        content: "./content/editAdmin",
        user: admin,
        admin: req.user.dataValues,
      });
    });
  };

  saveAdmin = async (req, res) => {
    const salt = await bcrypt.genSalt(10);
    Admin.update(
      {
        username: req.body.username,
        password: await bcrypt.hash(req.body.password, salt),
      },
      {
        where: { id: req.params.id },
      }
    ).then(res.redirect("/admin"));
  };

  deleteAdmin = (req, res) => {
    const id = req.params.id;
    Admin.destroy({
      where: { id: id },
    }).then(() => {
      res.redirect("/admin");
    });
    console.log(id);
  };

  getUser = (req, res) => {
    User.findAll({
      include: [
        {
          model: Biodata,
          as: "biodata",
        },
      ],
      order: [["createdAt", "DESC"]],
    }).then((users) => {
      res.render("index", {
        judul: "List User",
        content: "./content/listUser",
        users,
        admin: req.user.dataValues,
      });
    });
  };

  history = (req, res) => {
    History.findAll({
      include: [
        {
          model: User,
          as: "user",
        },
      ],
      order: [["createdAt", "DESC"]],
    }).then((history) => {
      res.render("index", {
        judul: "Daftar History",
        content: "./content/history",
        history,
        admin: req.user.dataValues,
      });
    });
  };
}

module.exports = HomeController;
