const { successResponse } = require("../../helpers/response");
const { User, History, Biodata } = require("../../models")

class UserController {
    getUser = (req,res) => {
        User.findAll().then((user) => {
            res.json(successResponse(res, 200, user, {
                total: user.length,
            }))
        })
    }
    
    getDetailUser = (req,res) => {
        User.findOne({
            where: { id: req.params.id},
            include: [
                {
                    model: History,
                    as: 'history',
                },
                {
                    model: Biodata,
                    as : 'biodata'
                }
            ],
        }).then((user) => {
            res.json(successResponse(res,201,user))
        });
    };

    insertBiodata = (req,res) => {
        Biodata.create(req.body)
        .then((user) => {
            res.json(successResponse(res,201,user))
        })
    } 

    insertScore = (req,res) => {
        History.create(req.body)
        .then((score) => {
            res.json(successResponse(res,201,score))
        })
    }

    deleteUser = (req, res) => {
        User.destroy({
          where: {
            id: req.params.id,
          },
        });
        History.destroy({
          where: {
            user_id: req.params.id,
          },
        });
        Biodata.destroy({
          where: {
            user_id: req.params.id,
          },
        }).then(() => {
          res.json(successResponse(res, 200, null));
        });
      };

      createRoom = (req, res) => {
        const room_id = req.body.room_id;
        const UserId = req.body.UserId;
        History.create({
          room_id,
          UserId,
        }).then((data) => {
          successResponse(res, 201, data);
        });
      };
    
      fightRoom = (req, res) => {
        const room_id = req.params.room;
        let data = {
          p1: req.body.player1,
          p1Choice: req.body.player1_choice,
          p2: req.body.player2,
          p2Choice: req.body.player2_choice,
          hasil: req.body.hasil
        };
        let result = [];
        result.push({data}).then((data) => {
          successResponse(res, 201, data)
        });
      }
      
    }

module.exports = UserController;